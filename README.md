## Video games!
See _instructions.txt files in the /src subdirectories of asteriods, mario, and sinistar. 

To run the three base-code games, right click the Game.java files in the mvc/controller/ dirs. 

To use either sinistar or mario as your base, delete the asteroids code in your proJava project and then
 drag and drop (to move) the mario or sinistar directories into your proJava project. IntelliJ will 
 automatically refactor the packages. Test the Game.java file by running it from your proJava. Right-click the new mario or 
 sinistar dir, select git | add files to git, then commit and push. Continue to modify the source code.
 





